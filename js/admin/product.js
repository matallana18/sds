$(document).ready(function () {

    var loading = '<div class="loading"></div>';

    function select_mini_image(my) {
        var scr = my.attr('src');
        var price = my.attr('rel-price');
        my.parents('article').find('.mini-image').css({'border': '1px solid #ddd'});
        my.parents('article').find('.main-image').attr({'src': scr});
        my.parents('article').find('.price').html(price);
        my.css({'border': '1px solid red'});
    }

    $('.mini-image').click(function () {
        var my = $(this);
        select_mini_image(my);
    });

    function filter() {

        var form = $('.form-filter');
        var url = form.attr('action');
        var data = form.serialize();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $.ajax({
            url: url,
            type: 'json',
            method: 'post',
            data: data,
            beforeSend: function () {
                // setting a timeout
                $('#content_products').append(loading);
            },
            success: function (response) {
                $('#content_products').html(response.html);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                console.log(jqXHR);
                var errors = jqXHR.responseJSON;
                $.each(errors, function (i, v) {
                    console.log(v);
                });
            },
            complete: function () {
                $('.loading').remove();
            }
        });
    }

    $('.form-filter').submit(function (e) {

        e.preventDefault();
        filter();

    });

    $('.page-link').click(function (e) {

        e.preventDefault();
        var page = $(this).html();
        $('input[name=page]').val(page);
        filter();
    });

    $('body').on('click','.btn.import', function(){
      var sku = $(this).attr('rel-sku');
      var url = "{{url('admin/product/import')}}";

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $.ajax({
            url: url,
            type: 'json',
            method: 'post',
            data: {sku : sku},
            beforeSend: function () {
                // setting a timeout
                $('#content_products').append(loading);
            },
            success: function (response) {
                $('#content_products').html(response.html);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                console.log(jqXHR);
                var errors = jqXHR.responseJSON;
                $.each(errors, function (i, v) {
                    console.log(v);
                });
            },
            complete: function () {
                $('.loading').remove();
            }
        });

    });

});
